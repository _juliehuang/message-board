import requests
from datetime import datetime
from elasticsearch import Elasticsearch, ElasticsearchException
import re
import sys
import logging

import es_ops
import config


es = Elasticsearch([{'host': config.ES_HOST, 'port': config.ES_PORT}])

# logging setup
logs = logging.getLogger('MainLogger')
logs.setLevel(logging.DEBUG)
logging.basicConfig(filename='main.log', filemode='w',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def createUser(username, password):

    user_info = {"username": username,
                 "password": password,
                 "date_created": datetime.now(),
                 "bad_posts": 0,
                 "is_banned": False}

    user = es_ops.userLookup(username)

    if user is None:  # if user does not exist
        results = es.index(index='users-index',
                           doc_type='_doc', refresh='true', body=user_info)
        logs.info(
            "Welcome {}! You have successfully created an account.".format(username))

    # user exists
    else:
        # if username exists in db then check if banned
        is_banned = es_ops.checkBan(username)

        if is_banned:
            raise Exception(
                "{} is banned. Please select a different username.".format(username))
        # otherwise, username is not banned but already exists
        raise Exception(
            "{} is already taken. Please try again".format(username))
    return user


# check word limit and censor, if greater than 3 then increase bad_posts count
def checkWordLimit(username, content):

    user = es_ops.userLookup(username)

    content_string = re.findall(r"[\w']+|[.,!?;-]+|[*]+", content)

    bad_words = 0

    for d in config.dictionary:
        for c in range(len(content_string)):
            if d == content_string[c]:
                bad_words += 1
                content_string[c] = "****"

    punct = set("!, ., -, ;")
    content = ''.join(w if set(w) <= punct else ' ' +
                      w for w in content_string).strip()

    num_bad_posts = user.get("_source").get("bad_posts")

    if bad_words >= config.WORD_LIMIT:
        num_bad_posts += 1
        user_id = user.get("_id")
        es.update(index="users-index", id=user_id,
                  body={"doc": {"bad_posts": num_bad_posts}})
        logs.info(
            "Too many offensive words. This message will not be posted.")
        raise Exception(
            "Too many offensive words. This message will not be posted")

    # print(content)
    return content


def postMessage(username, content):

    user = es_ops.userLookup(username)

    ban = es_ops.checkBan(username)

    content = checkWordLimit(username, content)

    if user is None:
        logs.error("There is no such account. Please create an account first.")
        raise Exception(
            "There is no such account. Please create an account first.")

    elif ban == True:
        logs.info("Message cannot be posted because you are banned.")
        raise Exception("Message cannot be posted because you are banned.")

    else:
        message_info = ({"username": username,
                         "content": content,
                         "date_posted": datetime.now(), })

        results = es.index(index='messages-index',
                           doc_type='_doc', body=message_info)

    return message_info


def getPost():
    post_id = input("Enter post id: ")
    try:
        res = es.search(index="messages-index",
                        body={"query": {"match": {"id": post_id}}})

        results = res["hits"]["hits"][0].get("_source")
        print(results)
    except:
        logs.error("Post number {} does not exist".format(post_id))
        raise Exception("Post number {} does not exist".format(post_id))


def getAllPosts():
    try:
        res = es.search(index="messages-index",
                        body={
                            "size": 100,
                            "query": {
                                "match_all": {}
                            },
                            "sort": {
                                "date_posted": {
                                    "order": "asc"
                                }
                            }
                        })
        return res["hits"]

    except ElasticsearchException as error:
        logs.error(error)
        raise Exception(error)


if __name__ == "__main__":
    # createUser("abc", "1234")
    # checkWordLimit("jayz", "I'm a bad bad liar guy")
    # postMessage("abc", "hello world")
    postMessage("bey", "i'm a star!")
    # postMessage("jayz", "hello friends")
    # postMessage("abc", 'horrible liar bad guy')
    # postMessage("jayz", 'hi')
    # postMessage("ella", 'bye')
    # getPost()
    # getAllPosts()
