from flask import Flask, jsonify, request, abort, render_template
import main
import es_ops

app = Flask(__name__, static_folder="./static/dist",
            template_folder="./static")


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/posts', methods=["POST", "GET"])
def posts():
    if request.method == 'POST':
        data = request.get_json()
        username = data["username"]
        content = data["content"]
        if not username or not content:
            abort(400, "You're missing username and/or content.")

        try:
            user = es_ops.userLookup(username)
            if user is None:
                register()
                posts()

            else:
                main.postMessage(username, content)

        except Exception as e:
            abort(400, e)

    try:
        all_posts = main.getAllPosts()
    except Exception as e:
        abort(500, e)
    return all_posts


@app.route('/users', methods=["POST"])
def register():
    data = request.get_json()
    username = data["username"]
    password = "password"
    if not username or not password:
        abort(400, "You're missing username and/or password.")

    try:
        main.createUser(username, password)
    except Exception as e:
        abort(400, e)

    return data


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)
