# user limitations
WORD_LIMIT = 3
POST_THRESHOLD = 3

# words to censor
dictionary = ["bad", "horrible", "liar"]

# es configs
ES_HOST = "es"
ES_PORT = 9200
