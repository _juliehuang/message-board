from elasticsearch import Elasticsearch, ElasticsearchException
import logging
import os

os.chdir('./schemas')

logs = logging.getLogger('ES_LOGGER')
logs.setLevel(logging.DEBUG)
logging.basicConfig(filename='es.log', filemode='w',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

es = Elasticsearch([{'host': 'localhost', 'port': 9200}])

if not es.ping():
    raise ValueError("Es connection failed")


def createIndex():
    for i in ['users', 'messages']:
        schema = open('es_{}_schema.json'.format(i)).read()
        try:
            index = es.indices.create(
                index='{}-index'.format(i), body=schema)
        except ElasticsearchException as error:
            logs.critical(error)

        try:
            if index["hits"]:
                logs.info("Sucessfully created {}-index".format(i))

        except:
            logs.critical("{}-index already exists".format(i))


if __name__ == "__main__":
    createIndex()
