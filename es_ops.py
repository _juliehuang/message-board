from elasticsearch import Elasticsearch

import config

es = Elasticsearch([{'host': 'es', 'port': 9200}])


def checkEs():
    if not es.ping():
        raise ValueError("Elasticsearch connection failed")

# looks up a given username in the database and returns it if it exists


def userLookup(username):

    # checks to see if es is up
    checkEs()

    user = es.search(index="users-index",
                     body={"query": {"match": {"username": username}}})

    if not user["hits"]["hits"]:
        user = None
        # print(user)
        print("User {} does not exist".format(username))

        # print(user)
    else:
        user = user["hits"]["hits"][0]
        # print(user)

    return user


def postLookup(message_id):
    checkEs()
    posts = es.search(index="messages-index")


def deletePostsByUser(username):
    checkEs()
    posts = es.search(index="messages-index",
                      body={"query": {"match": {"username": username}}})

    if posts["hits"]["hits"]:
        for count, doc in enumerate(posts):
            #print(count, doc)
            id_to_delete = posts["hits"]["hits"][count].get("_id")
            es.delete(index="messages-index", id=id_to_delete)
            print("deleting post id {}".format(id_to_delete))

    else:
        print(
            "There are no messages posted by {}".format(username))


# check if num posts meets threshold, if so remove all posts
def checkBan(username):
    checkEs()

    user = userLookup(username)
    print(user)

    # # user exists
    if user is not None:
        num_bad_posts = user.get("_source").get("bad_posts")
        user_id = user.get("_id")
        ban = user.get("_source").get("is_banned")

    else:
        raise Exception("There is no such user registered")

    if num_bad_posts >= config.POST_THRESHOLD:
        es.update(index="users-index", id=user_id,
                  body={"doc": {"is_banned": True}})
        deletePostsByUser(username)

    return ban


# if __name__ == "__main__":
    # userLookup("jay")
    # userLookup("ella")
    # checkBan("jayz")
    # checkBan("ella")
    # checkBan("jay")
    # deletePostsByUser("ella")
