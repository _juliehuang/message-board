# Message Board App

This app will allow users post messages on to the forum and to see posts made by other users. It will automatically censor inappropriate words which include bad, liar, horrible.  

The backend will be implemented using Python with Elasticsearch as the database to keep track of posts and users. 

## Functionality
Users will automatically be registered when they are making their first post.  An inappropiate post will contain 3 or more inappropriate words. Users will not be able to post such messages and will result in a strike on their user record. Once a user has tried to post 3 inappropriate posts, they will be banned and all their remaining posts on the forum will be deleted. 


## Prerequisites 

We assume that users have `python3`, `pip`, `npm`, and `Docker` already installed on their local machine.

## Setup

First, git clone the repository. 

To install all dependancies and create your environment for the app, run: 

```bash
make
```
which will start up the `flask` and `elasticsearch` containers and create your es indices. 

To see the posts navigate to `localhost:8000`



## To-Do
Fix error messages and include pagination.  

