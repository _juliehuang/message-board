HOST=127.0.0.1

all: es create_indices flask

init: 
	@echo "Installing packages and dependancies..." 
	pip install -r requirements.txt

es: 
	@echo "Starting Elasticsearch"
	docker-compose up -d es
	@echo "Waitng for Elasticseach ..."
	@sleep 30

indices: 
	@echo "Creating user, message indices..."
	@python3 create_es_indices.py

flask: 
	@ cd static && npm install && npm run build
	@docker build -t flask-app:latest .
	@docker-compose up -d flask

user: 
	@echo Adding user admin ...
	@curl -H "content-type: application/json" -XPOST -d '{"username":"admin","password":"123"}' localhost:8000/users

posts: 
	@echo Making a posts as admin ...
	@curl -H "content-type: application/json" -XPOST -d '{"username":"admin","content":"Hi friends! :) "}' localhost:8000/posts
	@curl -H "content-type: application/json" -XPOST -d '{"username":"admin","content":"bad guy - billie eilish"}' localhost:8000/posts
	@curl -H "content-type: application/json" -XPOST -d '{"username":"admin","content":"i like bad *** liars"}' localhost:8000/posts


clean: 
	@echo "Removing __pycache__, .swp files"
	@rm -r __pycache__
	@rm -r *.swp

.PHONY: es
	create_indices 
	clean
	flask
