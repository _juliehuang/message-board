import React from 'react';
import axios from 'axios';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        axios.get('http://localhost:8000/posts')
            .then(res => {
                console.log(res);
                this.setState({ data: res["data"] })
            })
    }

    render() {
        return ""
    }
}

export default App;