//App.jsx
import React from 'react';
import axios from 'axios';
import Post from './Post';
import { List, Button, Checkbox, Form } from 'semantic-ui-react'


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
        // this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        //event.preventDefault();
        const options = {
            headers: { 'content-type': 'application/json' }
        };
        axios.post('/posts', {
            username: event.target.username.value,
            content: event.target.content.value
        }, options)
            .then(res => {
                //console.log(res.data);
                //this.setState({ data: res.data["hits"] });
                if (!alert('A message has been posted sucessfully!')) { window.location.reload(); }
            })
            .catch(error => {
                //console.log(error.response.data)
                if (!alert(error.response.data)) { window.location.reload(); }
                console.error(error);
            })
        //alert('A new message has been posted!');

    }
    componentDidMount() {
        axios.get('http://localhost:8000/posts')
            .then(res => {
                console.log(res.data);
                this.setState({ data: res.data["hits"] });
            })
    }

    render() {

        const { data } = this.state;


        return (
            <div class="ui segment">
                <h2 class="ui block header">
                    <i class="envelope square icon"></i>
                    <div class="content center aligned">
                        Message Board
                    <div class="sub header">A fun and safe place to post your messages! 😊</div>
                    </div>
                </h2>

                {this.state.data && this.state.data.map(msg => {
                    return (
                        <List celled>
                            <List.Item>
                                <List.Content>
                                    <List.Header>{msg._source.username}</List.Header>
                                    {msg._source.content}
                                </List.Content>
                            </List.Item>
                        </List>
                    )
                })}
                <Form onSubmit={this.handleSubmit}>
                    <Form.Field>
                        <label>Username</label>
                        <input name="username" placeholder='Username' type='text' required />
                    </Form.Field>
                    <Form.Field>
                        <label>Message</label>
                        <input name="content" placeholder='Message' type='text' required />
                    </Form.Field>
                    <input type="submit" value="Submit" class="ui teal button big" />
                </Form>
            </div>
        )
    }
}

export default App;
